#As part of this project I created application load balancer with ec2 instance where min-2 andmax 4 ande desired capacity is 2. If any issue happen to any one of the ec2 instance then ALB automatically troggers new ec2 instance and it will be up and running within seconds.

cd existing_repo
git init
git remote add origin https://gitlab.com/aws-cdk-typescript/apploadbalancer.git

git branch -M main
git add .
git commit -m "my first commit message"
git branch --set-upstream-to=origin/main
Allowed to force push
https://gitlab.com/aws-cdk-typescript/aws-cloudwatch/-/settings/repository#js-protected-branches-settings

git push -uf origin main

# Welcome to your CDK TypeScript project

This is a blank project for CDK development with TypeScript.

The `cdk.json` file tells the CDK Toolkit how to execute your app.

## Useful commands

* `npm run build`   compile typescript to js
* `npm run watch`   watch for changes and compile
* `npm run test`    perform the jest unit tests
* `cdk deploy`      deploy this stack to your default AWS account/region
* `cdk diff`        compare deployed stack with current state
* `cdk synth`       emits the synthesized CloudFormation template
