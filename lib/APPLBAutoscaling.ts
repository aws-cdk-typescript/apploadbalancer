import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as ec2 from 'aws-cdk-lib/aws-ec2';
import * as autoscalling from 'aws-cdk-lib/aws-autoscaling'
import * as alb from 'aws-cdk-lib/aws-elasticloadbalancingv2'
import { listenerCount } from 'process';

export interface CustomeProps{
    // List all properties
    name: string
}

   /*Usage autoscalling construct
    ** Autoscalling contruct
    */
export class Autoscalling extends Construct {
    public constructor(stack: cdk.Stack, id: string){
        super(stack, id)    

    /*Usage ec2 vpc creation
    ** Default VPC names MyCdkVPC creation
    */
    const vpc = new ec2.Vpc(this, 'MyCdkVPC');
    
    /*Usage Autoscalling group
    ** Autoscalling MyASG creation
    */
    const asg = new autoscalling.AutoScalingGroup(this, 'MyASG', {
        vpc, 
        instanceType: ec2.InstanceType.of(ec2.InstanceClass.T2, ec2.InstanceSize.MICRO),
        machineImage: new ec2.AmazonLinuxImage(),
        minCapacity:1,
        maxCapacity:4,
        desiredCapacity:2
    })

    // Applocation Load balancer create with internet facing true
    const lb = new alb.ApplicationLoadBalancer(this, 'MyLB',{
        vpc,
        internetFacing: true       
    });

    // Application Load balancer listens on port 80
    const listener = lb.addListener('listener', {
        port: 80
    });

    //Load balance incoming requests to the given load balancing targets.
    listener.addTargets('Target', {
        port: 80,
        targets: [asg]
    });

    //Allow default connections from all IPv4 ranges
    listener.connections.allowDefaultPortFromAnyIpv4('Open to access from anywhere');

    //Scale out or in to achieve a target request handling rate
    asg.scaleOnRequestCount('TestLoad',{
        targetRequestsPerMinute: 60,
    });

    //
    /**
     * The following example scales to keep the CPU usage of your instances around 50% utilization:
     * Scale out or in if cpu utlization is 5
     */
    asg.scaleOnCpuUtilization('AsgCPU',{
        targetUtilizationPercent: 5,
    });
    }
}